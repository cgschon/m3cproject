#Christopher Schon 00734692
"""Project part 2
Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack
"""
from p2 import fdmodule2d as f2d
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

def threshold(M,fac,display = True,savefig = True):
    """set all elements of M to zero where grad_amp < fac*max(grad_amp)
    Since fac is a proportion of the maximum, it must be betweeen 0 and 1.
    If display is set as true it will display the final figure. 
    If savefig is true then the figure will be saved as p2.png.
    """
    assert fac >= 0 and fac <= 1, "error, fac must be in (0,1)"
    
    f2d.dx1 = 1
    f2d.dx2 = 1
    
    f2d.n1 = np.shape(M)[1]
    f2d.n2 = np.shape(M)[0]
    amps = np.zeros(np.shape(M))
    dfmax = np.zeros(3)
    for i in range(0,3):
        amps[:,:,i], dfmax[i] = np.array(f2d.grad_omp(M[:,:,i]))[[2,3]]
         
    N = M
    N[(amps[:,:,0] < fac*dfmax[0]) | (amps[:,:,1] <fac*dfmax[1]) | (amps[:,:,2] <fac*dfmax[2])] = 0.0
    
    if(display):
        plt.imshow(N)
        
    if(savefig):
        plt.savefig('p2.png')
        
    
    
    return N,dfmax

    

if __name__ == '__main__':
    M=misc.face()
    print "shape(M):",np.shape(M)
    plt.figure()
    plt.imshow(M)
    plt.show() #may need to close figure for code to continue
    N,dfmax=threshold(M,0.2,True,True) #Uncomment this line 
    plt.show()
    