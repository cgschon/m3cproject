# README #


### What is this repository for? ###

This repository contains fortran modules and python scripts designed to:
 -Find the gradient of one/two dimensional functions in serial and parallel using finite-difference methods of second and fourth order. Parallel code is designed using both OpenMp and MPI. 

 -Apply the above to a gradient thresholding program in Python.

 -Apply the code to one dimensional and two dimensional advection equation problems, comparing serial and parallel accuracy and speeds.
 

Modified/created files:

-cfd4_2d.f90: modified so that it performs the serial cfd4 version in a more efficient manner.

-grad2d.f90: same functionality as grad, but uses the cfd4_2d routine instead. 

-p1_3.py: test python script. Compares 2d and 1d methods described above.

-p2.py: Applies the gradient code developed to a gradient thresholding problem.
 
-ode_v2.py: main fortran code used to solve the 1d advection equation.

-p3_v2.py: solves advection equation using serial and parralel code for user choice of c and S. Plots the solutions and prints the errors and error ratios.

-ode_mpi: solves the advection equation in parellel using MPI.

-fmpi.dat: data file produced by euler_mpi.

-fmodule2dp.f90: module containing gradient finding sub routines used in ode2d_v2.f90. 

-ode2d_v2.f90: module containing rk4 and Euler methods for solving 2d advection equation in serial and in parallel (OpenMP).

-p4.py: test program for ode2d_v2.f90. Tests speed and accuracy of serial and parallel code. Produces several plots.

-Speedupp4.png, nt_times.png, nx_times.png: All plots produced by p4.py, illustrating speed differences between serial and parallel code. 