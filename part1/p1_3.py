#Christopher Schon 00734692
"""
Test gradient routines in fdmodule2d
To build the .so module:
rm *.mod
gfortran -c fdmodule.f90 fdmodule2d.f90
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p1_3 -llapack
"""
from p1_3 import fdmodule as f1
from p1_3 import fdmodule2d as f2d
import numpy as np
import matplotlib.pyplot as plt

def test_grad1(n1,n2,numthreads):
    """call fortran test routines, test_grad,test_grad_omp,teat_grad2d
    with n2 x n1 matrices, run f2d_test_grad_omp with numthreads threads
    return errors and computation times
    """
    f2d.n1 = n1
    f2d.n2 = n2
    f2d.dx1 = 2./(n1-1)
    f2d.dx2 = 2./(n2-1)
    f2d.numthreads = numthreads
    
    error = np.zeros((10,2))
    errorp = np.zeros((10,2))
    error2d = np.zeros((10,2))
    time = np.zeros(10)
    timep = np.zeros(10)
    time2d = np.zeros(10)
    
    for i in range(0,10):
        error[i,:], time[i] = f2d.test_grad() 
        errorp[i,:], timep[i] = f2d.test_grad_omp()
        error2d[i,:], time2d[i] = f2d.test_grad2d()
    #need to add e2d and t2d
    
    return error, errorp, error2d, np.mean(time), np.mean(timep), np.mean(time2d) 
    
    
def test_gradN(numthreads):
    """input: number of threads used in test_grad_omp
    call test_grad1 with a range of grid sizes and
    assess speedup
    2d solver is faster due to less overhead (only one call of dgtsv is needed 
    compared for each axis compared to calling for each row/col vector).
    """
    
   #loop through 50<n1,n2<1600 storing speedup
    n1values = np.array([50, 100, 200, 400, 800, 1600, 3200])
    n2values = n1values
    nn = np.size(n1values)
    speedup1 = np.empty((nn,nn))
    speedup2 = np.empty((nn,nn))
    
    for i in enumerate(n1values):
        for j in enumerate(n2values):
            print "running with n1,n2=",i[1],j[1]              
            f2d.n1=i[1]
            f2d.n2=j[1]
    
            e,ep,e2d,t,tp,t2d = test_grad1(f2d.n1,f2d.n2,2)
            speedup1[i[0],j[0]] = t/tp #compute and store speedup
            speedup2[i[0],j[0]] = t/t2d

    #display results
    plt.figure()
    plt.plot(n1values,speedup1[:,::2])
    
    plt.legend(('n2=50','200','800','3200'),loc='best')
    plt.xlabel('n1')
    plt.ylabel('speedup')
    plt.title('Speedup Serial/Parallel Time, Christopher Schon, test_gradN')
    plt.axis('tight')
    
    plt.figure()
    plt.plot(n1values,speedup2[:,::2])
    plt.legend(('n2=50','200','800','3200'),loc='best')
    plt.xlabel('n1')
    plt.ylabel('speedup')
    plt.title('Speedup 1d/2d Time, Christopher Schon, test_gradN')
               
    return speedup1,speedup2

if __name__ == "__main__":
    
    e,ep,e2d,t,tp,t2d = test_grad1(400,200,2)
    s,s2d = test_gradN(2) 
    plt.show()
