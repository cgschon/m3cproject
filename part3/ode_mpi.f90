!Christopher Schon 00734692
!mpif90 -O3 -o adv_mpi.exe ode_mpi.f90
!to run mpiexec -n 2 adv_mpi.exe (using 2 processes in this example, could be any other number)
module advmodule
    implicit none
    real(kind=8) :: S_adv,c_adv
end module advmodule
!-------------------------------
program advection_mpi
    use mpi
    use advmodule
    implicit none
    integer :: i1,j1
    integer :: nt,n !number of time steps, number of spatial points
    real(kind=8) :: dt,dx,pi !time step, grid size, advection eqn parameters
    integer :: myid, numprocs, ierr
    real(kind=8), allocatable, dimension(:) :: x,f0,f !grid, initial condition, solution
    
 ! Initialize MPI
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

!gather input
    open(unit=10,file='data_mpi.in')
        read(10,*) n
        read(10,*) nt
        read(10,*) dt
        read(10,*) c_adv
        read(10,*) S_adv
    close(10)

    allocate(x(n),f0(n),f(n))

!make grid from 0 to 1-dx, dx = 1/n
    do i1=1,n
        x(i1) = dble(i1-1)/dble(n)
    end do
    dx = 1.d0/dble(n)

!generate initial condition
    pi = acos(-1.d0)
    f0 = sin(2.d0*pi*x)


!compute solution
    call euler_mpi(MPI_COMM_WORLD,numprocs,n,0.d0,f0,dt,nt,dx,f)


!output solution
        call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
       if (myid==0) then
        print *, 'max(f)=',maxval(abs(f))
        open(unit=11,file='fmpi.dat')
        do i1=1,n
            write(11,*) f(i1)
        end do
        close(11)
    end if
    !can be loaded in python with: f=np.loadtxt('fmpi.dat')

    call MPI_FINALIZE(ierr)
end program advection_mpi
!-------------------------------


subroutine euler_mpi(comm,numprocs,n,t0,y0,dt,nt,dx,y)
    !explicit Euler method
    use mpi
    use advmodule
    implicit none
    integer, intent (in) :: n,nt
    real(kind=8), dimension(n), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt,dx
    real(kind=8), dimension(n), intent(out) :: y
    real(kind=8), dimension(:), allocatable :: ylocal,Rpart, ytemp
    real(kind=8) :: t
	integer :: i1,k,istart,iend
    integer :: comm,myid,ierr,numprocs, sender, receiver, nlocal
    logical, parameter :: display = .false.
    integer, dimension(MPI_STATUS_SIZE) :: status

    integer, allocatable, dimension(:) :: Nper_proc, disps


    call MPI_COMM_RANK(comm, myid, ierr)
    print *, 'start euler_mpi, myid=',myid

    !set initial conditions
    y = y0
    t = t0

    !generate decomposition and allocate sub-domain variables
    call mpe_decomp1d(size(y),numprocs,myid,istart,iend)
    nlocal = iend-istart+1
    print *, 'istart,iend,threadID,nlocal=',istart,iend,myid,nlocal
    
    !allocate appropriate sizes for temp/local variables
    allocate(ylocal(nlocal),Rpart(nlocal), ytemp(0:nlocal+1))
    !nlocal = size(ylocal)
    !ylocal is the part of y from istart to iend
    ylocal = y(istart:iend)
    !print *,'ylocal=',size(ylocal)
    ytemp(1:nlocal) = ylocal
    !print *,'ytemp=', size(ytemp)
    !time marching
    do k = 1,nt

    
        !want: to form a temporary y with endpoints so that RHS_mpi can use it and put it back in to y
        ! copy non-end points of ylocal into ytemp(1:nlocal).. ytemp has size nlocal +2
        ytemp(1:nlocal) = ylocal
        
        if (myid<numprocs-1) then
            receiver = myid+1
        else
            receiver = 0
        end if
    
        if (myid>0) then
            sender = myid-1
        else
            sender = numprocs-1
        end if
        !send ylocal(nlocal) up to the processor above's ytemp(0)
        call MPI_SEND(ylocal(nlocal),1,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr)
        !and receive this into ytemp(0) from sender below
        call MPI_RECV(ytemp(0),1,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)
    
        

        if (myid>0) then
            receiver = myid-1
        else
            receiver = numprocs-1
        end if
    
        if (myid<numprocs-1) then
            sender = myid+1
        else
            sender = 0
        end if
        !send bottom ylocal value down a processor
        call MPI_SEND(ylocal(1),1,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr)
        !receive from processor above their ylocal(1) into your temp
        call MPI_RECV(ytemp(nlocal+1),1,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)
        
        
        !print *, 'ytemp(0), ytemp(nlocal) =',ytemp(0),ytemp(nlocal)
        !call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        !all processes completed sending data up and down
        !print *, 'id,ylocal=', myid,ylocal
        !print *, 'ylocal =', ylocal
        !print *, 'id,ytemp=', myid,ytemp
        !print *, 'ytemp =', ytemp
        !now use the ytemp with end points to calculate derivative and put into Rpart
        call RHS_mpi(nlocal+2,dx,t,ytemp,Rpart)
        !print *,'id,rpart=',myid,Rpart
        !modify ylocal by adding dt*Rpart
        ylocal= ylocal + dt*Rpart !ylocal must be declared and defined, Rpart must be declared, and 
                                  !should be returned by RHS_mpi
        t = t + dt
        call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    end do

    print *, 'before collection',myid, maxval(abs(ylocal))
    !collect ylocal from each processor onto myid=0
    
    allocate(Nper_proc(numprocs),disps(numprocs))

    !gather nlocal from each proc to array Nper_proc on myid=0
    call MPI_GATHER(nlocal,1,MPI_INT,Nper_proc,1,MPI_INT,0,comm,ierr)
    if (myid==0) then
        if (display) print *,'Nper_proc=',Nper_proc
        disps(1)=0
        do i1=2,Numprocs
            disps(i1) = disps(i1-1)+Nper_proc(i1-1) !needed for gatherv below
        end do
        if (display) print *, 'disps=', disps
    end if

    !collect df from each processor onto myid=0
    call MPI_GATHERV(ylocal,nlocal,MPI_DOUBLE_PRECISION,y,Nper_proc, &
                disps,MPI_DOUBLE_PRECISION,0,comm,ierr)
    if (myid==0) print *, 'finished',maxval(abs(y))



end subroutine euler_mpi
!-------------------------
subroutine RHS_mpi(nn,dx,t,f,rhs)
    use advmodule
    implicit none
    integer, intent(in) :: nn
    real(kind=8), intent(in) :: dx,t
    real(kind=8), dimension(nn), intent(in) :: f
    !output variable rhs must be declared
    real(kind=8), dimension(nn-2), intent(out) :: rhs

    rhs = (0.5d0/dx)*(f(3:)-f(:size(f)-2))
    rhs = S_adv - c_adv*rhs

end subroutine RHS_mpi

!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D


