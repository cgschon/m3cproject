#Christopher Schon 00734692
"""Project part 3, solve the 1d advection equation in fortran"""
import numpy as np
import matplotlib.pyplot as plt
import adv

#gfortran -c fdmoduleB.f90 ode.f90 advmodule.f90
#The above step should generate three .mod files
#f2py -llapack -c fdmoduleB.f90 ode.f90 advmodule.f90 -m adv --f90flags='-fopenmp' -lgomp

def advection1f(nt,tf,n,dx,c=1.0,S=0.0,display=False,numthreads=1):
    """solve advection equation, df/dt + c df/dx = S
    for x=0,dx,...,(n-1)*dx, and returns f(x,tf),fp(x,tf),
    and f4(x,tf) which are solutions obtained using the fortran
    routines ode_euler, ode_euler_omp, ode_rk4
    -    f(x,t=0) = sin(2 pi x/L), L = n*dx
    -    nt time steps are taken from 0 to tf
    -    The solutions are plotted if display is true
    """
    pi = np.pi
    adv.advmodule.c_adv = c
    adv.advmodule.s_adv = S
    adv.fdmodule.n = n
    adv.fdmodule.dx = dx
    
    f = np.zeros(n)
    fp = np.zeros(n)
    f4 = np.zeros(n)
    
    x = np.linspace(0,(n-1)*dx,n)
    y0 = np.sin(2*pi*x/(n*dx))
    t0 = 0.0
    dt = tf/nt
    
    f = adv.ode.euler(t0,y0,dt,nt)
    f4 = adv.ode.rk4(t0,y0,dt,nt)
    fp = adv.ode.euler_omp(t0,y0,dt,nt,2)
    if(display):
        plt.figure()
        plt.title('Solutions formed by advection1f, Christopher Schon')
        plt.xlabel('x')
        plt.ylabel('f(x)')
        plt.plot(x,f, label = 'ode_euler')
        plt.plot(x,f4, label = 'ode_rk4')
        plt.plot(x,fp, label = 'ode_euler_omp')
        plt.legend(loc = 'best')
    
    return f,fp,f4 

 
def test_advection1f(n):
    """compute scaled L1 errors for solutions with n points 
    produced by advection1f when c=1,S=1,tf=1,nt=16000, L=1"""
    pi = np.pi
    c = 1.0
    S = 1.0
    adv.advmodule.c_adv = c
    adv.advmodule.s_adv = S
    nt = 16000.0
    tf = 1.0
    L = 1.0
    dx = L/n
    x = np.linspace(0,(n-1)*dx,n)
   
    f,fp,f4 = advection1f(nt,tf,n,dx,c,S)
    
    truef = np.sin(2*pi*(x-c*tf)) + S*tf
    
    errorf = (1.0/n)*np.sum(abs(f-truef))
    errorf4 = (1.0/n)*np.sum(abs(f4-truef))
    errorfp = (1.0/n)*np.sum(abs(fp-truef))
    #truedfdt = -2*pi*c*np.cos(2*pi*(x-c*t)) + S
    #truedfdx = 2*pi*np.cos(2*pi*(x-c*t))
    
    return errorf, errorfp, errorf4
    #errors from euler, euler_omp, rk4
        
"""This section is included for assessment and must be included as is in the final
file that you submit,"""
if __name__ == '__main__':
    n = 200
    nt = 320000
    tf = 1.21
    dx = 1.0/n
    f,fp,f4 = advection1f(nt,tf,n,dx,1,0,True)
    e,ep,e4 = test_advection1f(200)
    eb,epb,e4b = test_advection1f(400)
    print e,ep,e4
    print eb,epb,e4b
    print e/eb,ep/epb,e4/e4b
    plt.show()