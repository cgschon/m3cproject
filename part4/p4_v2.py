#Project part 4
#Christopher Schon 00734692
import numpy as np
import matplotlib.pyplot as plt
import adv2d
#gfortran -c advmodule2d.f90
#f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fdmodule2dp.f90 ode2d_v2.f90 -m adv2d --f90flags='-fopenmp' -lgomp


#add functions as needed

def rk4run(t0,f0,dt,nt,par,numthreads):
    """runs rk4 with optional par and numthreads parameters"""
    adv2d.ode2d.numthreads = numthreads
    adv2d.ode2d.par = par
    adv2d.fdmodule2d.numthreads = numthreads
    

    run = adv2d.ode2d.rk4(t0,f0,dt,nt)
    
    return run 
def advection2f(nt,tf,nx,ny,dx,dy,c1=1.0,c2=1.0,S=1.0,display=False,numthreads=1):
    """solve advection equation, df/dt + c1df/dx + c2dy/dx = S
    for x=0,dx,...,(nx-1)*dx, y = 0,dy,...,(ny-1)*dy and returns fe, frk4, frk4p
    which are solutions obtained using the fortran
    routines ode_euler, ode_rk4, parralell version of ode_rk4
    -    f(x, y, t=0) = exp(-100*((x-x0)^2 + (y-y0)^2))
    -    nt time steps are taken from 0 to tf
    -    The solutions are plotted if display is true
    """
    x0 = 0.5
    y0 = 0.5
    
    adv2d.advmodule.c1_adv = c1
    adv2d.advmodule.c2_adv = c2
    adv2d.advmodule.s_adv = S
    adv2d.ode2d.numthreads = numthreads 
    adv2d.fdmodule2d.numthreads = numthreads 
    adv2d.fdmodule2d.n1 = nx
    adv2d.fdmodule2d.n2 = ny
    adv2d.fdmodule2d.dx1 = dx
    adv2d.fdmodule2d.dx2 = dy
    
    f0 = np.zeros((nx,ny))
    frk4 = np.zeros((nx,ny))
    frk4p = np.zeros((nx,ny))
    
    
    x = np.linspace(0,(nx-1)*dx,nx)
    y = np.linspace(0,(ny-1)*dy,ny)
    xx, yy = np.meshgrid(x,y)
    
    f0 = np.exp(-100*(pow(xx-x0,2)+pow(yy-y0,2)))
    t0 = 0.0
    dt = tf/nt
    
    adv2d.ode2d.par = 0.0

    #frk4 = adv2d.ode2d.rk4(t0,f0,dt,nt)
    frk4 = rk4run(t0,f0,dt,nt, 0, 1)
    
    adv2d.ode2d.par = 1.0
    
    frk4p  = rk4run(t0,f0,dt,nt, 1, 2)
    #frk4p = adv2d.ode2d.rk4(t0,f0,dt,nt)

    truef = f0 + S*2
    
    if(display):
        
        plt.figure()
        CS = plt.contour(x,y,frk4[0])
        plt.title('rk4 contour plot, advectionf2, Christopher Schon')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.clabel(CS, inline=1, fontsize=10)
        plt.figure()
        CS2 = plt.contour(x,y,frk4p[0])
        plt.title('rk4 parallel contour plot, advectionf2, Christopher Schon')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.clabel(CS2, inline = 1, fontsize = 10)
    
    return truef, frk4, frk4p
    
def test_advection2f(nx,ny):
    """  
    test function for advection2f for nx by ny grid size. 
    tf is set as 1.
    Plots times for increasing nt for serial and parallel code.
    Plots times for increasing square grids.
    Plots speedup for varying grid sizes. 
    We see small speedup when nx, ny are small, since parallel code is not
    better than vectorised serial versions for smaller sized vectors.
    However as we increase the grid size to lareger problems the benefits of 
    parallel are seen by an increased speedup. 
    Increased nt do not see as large an effect from the speedup, since the size 
    of the problem is not increased significantly when nt is increased. 
    """

    nts = [200,400,800,1600]
    dx = 1./nx
    dy = 1./ny
    
    tf = 1.0
    errors = np.zeros((4,2))
    errors2 = np.zeros((4,2))
    times = np.zeros((4,2,3))
    times2 = np.zeros((4,2,3))
    
    #errors gives errors for nt, times gives times for varying nt
    
    for idx, v in enumerate(nts):
        nt = v
        truef = advection2f(nt,tf,nx[2],ny[2],dx[2],dy[2], display = False, numthreads = 2)[0]
        for i in np.arange(0,2):
            for j in np.arange(0,3):
                f,t = advection2f(nt,tf,nx[2],ny[2],dx[2],dy[2], numthreads = 2)[i+1]
                times[idx,i,j] = t
                
            errors[idx,i] = (1./(nx[2]*ny[2]))*(np.sum(np.sum(abs(f - truef))))
            
   #plot speedups for varying nt
    plt.figure()
    plt.title('advection2f Time vs Nt, nx = ny = 200, by Christopher Schon')
    plt.plot(nts, np.mean(times,axis=2)[:,0], label = 'Serial')
    plt.plot(nts, np.mean(times,axis=2)[:,1], label = 'Parallel')
    plt.legend(loc = 'best')
    plt.xlabel('nt')
    plt.ylabel('Time')
    plt.savefig('nt_times.png')
    
   #errors2 gives error for varying nx, ny, times2 gives times for varying nx,ny
    for idx, v in enumerate(nx):
        #set nt fixed at 800 and get truef
        nt = 800
        truef = advection2f(nt,tf,v,v,dx[idx],dy[idx], display = False, numthreads = 2)[0]
        #for serial and parallel do 3 times to take average
        for i in np.arange(0,2):
            for j in np.arange(0,3):
                f,t = advection2f(nt,tf,v,v,dx[idx],dy[idx], numthreads = 2)[i+1]
                times2[idx,i,j] = t
                
            errors2[idx,i] = (1./(nx[idx]*ny[idx]))*(np.sum(np.sum(abs(f - truef))))
            
    plt.figure()
    plt.title('advection2f Times for Square Grids, nt = 800 by Christopher Schon')
    plt.plot(nx, np.mean(times2,axis=2)[:,0], label = 'Serial')
    plt.plot(nx, np.mean(times2,axis=2)[:,1], label = 'Parallel')
    plt.legend(loc = 'best')
    plt.xlabel('nx (=ny)')
    plt.ylabel('Time') 
    plt.savefig('nx_times.png') 
        
    t = np.zeros(2)
    speedup = np.zeros((4,4))
    nt = 800
    for idx, x in enumerate(nx):
        for idy, y in enumerate(ny):
            for k in np.arange(0,2):
                t[k] = advection2f(nt,tf,x,y,1./x,1./y,numthreads=20)[k+1][1]
            speedup[idx,idy] = t[0]/t[1]
            
    plt.figure()
    plt.title('advection2f Speedup with nt = 800, by Christopher Schon')
    plt.plot(nx, speedup)
    plt.legend(('ny=50','100','200','400'),loc='best')
    plt.xlabel('nx')
    plt.ylabel('Speedup')
    plt.savefig('Speedupp4.png')
            
    return errors,np.mean(times, axis = 2), errors2, np.mean(times2,axis =2)
            
    
if __name__=='__main__':
    nx = 200
    ny = 200
    nt = 1600
    tf = 1.0
    dx = 1.0/nx
    dy = 1.0/ny
    truef, frk4, frk4p = advection2f(nt,tf,nx,ny,dx,dy, display = True, numthreads = 2)
    errors, times, errors2, times2 = test_advection2f(np.array([50,100,200,400]),np.array([50,100,200,400]))
    plt.show()
    #add code as needed
